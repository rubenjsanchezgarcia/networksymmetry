function A = Decompression( B, S, a)
%DECOMPRESSION Decompress matrix compressed by SymmetryCompression
%
%   Lossless compression:
%   If [ B, S, a ] = SymmetryCompression( A, motifs, motiftype ) then 
%   A = Decompress( B, S, a) up to rounding errors
%
%   Average compression:
%   If [ B, S] = SymmetryCompression( A, motifs ) then 
%   A = Decompress( B, S) up to rounding errors except intra-motif edges
%   which are replaced by average values

%start elapsed time
tic;

%average recovery
aux = S*sparse(diag(sum(S).^(-1)));
A = aux*B*aux.';

%lossless recovery
if nargin > 2
    %beta
    nto = find(sum(S)>1);   %non-trivial orbits
    for i = 1:length(nto)
        %orbit and its size
        or = find(S(:,nto(i)));  
        l = length(or);     
        %construct block matrix
        beta = a.beta(i);
        alpha = (B(i,i)/l-beta)/(l-1);
        block = alpha*(ones(l)-eye(l))+beta*eye(l);
        %assign block matrix
        A(or,or) = block;
    end
    %delta
    for i = 1:length(a.delta)
        %orbits and length
        or1 = find(S(:,a.deltapos(i,1)));
        or2 = find(S(:,a.deltapos(i,2)));
        l = length(or1);
        %construct block matrix
        delta = a.delta(i);
        gamma = (B(a.deltapos(i,1),a.deltapos(i,2))/l-delta)/(l-1);
        block = gamma*(ones(l)-eye(l))+delta*eye(l);
        %permute second orbit, if applicable
        perm = a.deltaperm(or2);
        if ~isempty(find(perm,1))
            or2 = or2(a.deltaperm(or2));
        end
        %assign block matrix
        A(or1,or2) = block;
        A(or2,or1) = block;
    end
end

%elapsed time
toc;

end

