function [ B, S, a ] = SymmetryCompression( A, motifs, motiftype )
%SYMMETRYCOMPRESSION Compress matrix by eliminating symmetries
%
%   [B, s] = SymmetryCompression(A, motifs) constructs characteristic
%   matrix of the partition S and returns B=S.'*A*S (unweighted quotient) 
%   and characteristic matrix of the partition S
%
%   [B, s, a] = SymmetryCompression(A, motifs, motiftype) performs lossless 
%   compression by restricting to orbits of BSMs up to 2 orbits, and, in 
%   addition, returns (symmetric) sparse annotation matrix a
%
%   A is the matrix representation of a (symmetric) network function
%
%   motifs and motiftype can be obtained with ReadGD as 
%   [ ~, ~, motiftype, motifs ] = ReadGD( 'filename' )
%

%check A symmetric
if ~isempty(find(A-A.',1))
    error('Input matrix must be symmetric');
end

%start elapsed time
tic;

%select orbits (if lossless compression) and convert to orbit partition
if nargin > 2
    fprintf('Lossless compression. ');
    bsm = motifs(motiftype>0 & cellfun(@length,motifs)<3);  %extract BSMs
    partition = [bsm{:}];
else
    fprintf('Average compression. ')
    partition = [motifs{:}];
end

%characteristic matrix 
S = CharacteristicMatrix(partition,size(A,1));

%unweighted quotient matrix
B = S.'*A*S;    

%annotations (if lossless compression)
if nargin > 2
    %beta
    nto = find(sum(S)>1);           %non-trivial orbits
    beta = zeros(length(nto),1);    %initialise beta
    for or = nto                    %for each non-trivial orbit
        rep = find(S(:,or),1);      %find orbit representative (first element)
        beta(or) = A(rep,rep);      %store annotation (beta)
    end
    %delta
    bsm2 = bsm(cellfun(@length,bsm)>1); %BSMs 2 orbits
    l = length(bsm2);
    delta = zeros(l,1);             %stores delta values
    deltapos = zeros(l,2);          %stores orbits
    deltaperm = zeros(size(A,1),1); %stores second orbit permutations
    for k = 1:l
        rep1 = min(bsm2{k}{1});     %first orbit representative
        rep2 = min(bsm2{k}{2});     %second orbit representative
        or1 = find(S(rep1,:),1);    %first orbit number
        or2 = find(S(rep2,:),1);    %second orbit number
        deltapos(k,:) = [or1 or2];  %store orbits numbers
        orbit1 = logical(S(:,or1)); %vertices in first orbit
        orbit2 = logical(S(:,or2)); %vertices in second orbit
        c = full(A(rep1,orbit2));   %candidates to delta value
        %choose delta as non-repeating value in c
        diff = c - A(rep1,rep2);    %vectors of differences
        if nnz(diff) == 1
            delta(k) = c(find(diff,1));
        else
            %this case also deals with delta==gamma
            delta(k) = full(A(rep1,rep2)); 
        end
        %store second orbit permutation
        if ~isempty(find(diff,1))         %if delta ~= gamma
            [perm,~] = find(A(orbit1,orbit2)==delta(k));
            [~,deltaperm(orbit2)] = sort(perm); 
        end
    end
    %construct annotation structure
    a = struct('beta',beta,'delta',delta,'deltapos',deltapos,...
        'deltaperm',sparse(deltaperm));
end

%compression ratio and elapsed time
if nargin > 2
    cr = (nnz(B)+nnz(sum(S)-1)+length(beta)+length(delta)+...
        length(deltapos)+nnz(deltaperm))/nnz(A)*100;
else
    cr = (nnz(B)+nnz(sum(S)-1))/nnz(A)*100;
end
fprintf('Compression ratio -> %.2f%%\n', cr);
toc;

end