function [ U, D, Uq, Dq, t ] = SpectralDecomposition( A, motifs, S, dp )
%SPECTRAL DECOMPOSITION Eigendecomposition via symmetries
%
%   [U, D] = SpectralDecomposition(A, motifs) returns orthogonal matrix U
%   of eigenvectors and diagonal matrix D of eigenvalues so that A*U = U*D
%    
%   A must be a symmetric matrix and motifs a list of symmetric motifs of
%   the underlying symmetries of A
%
%   For example, A could be the matrix reprentation of a pairwise network
%   function (Laplacian, exponential, etc) and motifs the list of symmetric
%   motifs such as [ ~, ~, ~, motifs ] = ReadGD( 'filename' )
%

%control flags
checkOutput = false;
verbose = true;

%decimal places for rounding 
if nargin < 4, dp = 10; end

%check A symmetric
if ~isempty(find(A-A', 1))
    error('Input matrix must be symmetric');
end

%start stopwatch
tic;

%characteristic matrix of the partition into orbits
n = size(A,1);
if nargin < 3 
    S = CharacteristicMatrix([motifs{:}],n);
end

%initialise U and D
U = zeros(n);   
D = zeros(n); 

%orbit sizes
s = sum(S);

%symmetric quotient
lambda = diag(s.^(-1/2));
aux = S * lambda;
Bsym = round(aux.' * A * aux, dp);

%spectral decomposition of the symmetric quotient
if verbose
    fprintf('Spectral decomposition of the symmetric quotient...');
end
[Uq, Dq] = eig(full(Bsym));
if verbose, fprintf('Finished.\n'); toc; end

%convert to eigenvectors of the left quotient
Uq = lambda * Uq;

%assign Uq, Dq to U, D
m = size(S,2);
U(:,1:m) = S * Uq;
D(1:m,1:m) = Dq;

%redundant spectrum
if verbose, fprintf('Redundant spectrum of the symmetric motifs...'); end
for i = 1:length(motifs)
    %orbits and vertices in symmetric motif
    orbits = motifs{i};
    sm = [orbits{:}];
    %motif adjacency matrix, spectrum and characteristic matrix
    Asm = full(A(sm,sm));
    [Usm, Dsm] = eig(Asm);
    Ssm = S(sm,:);
    Ssm = Ssm(:,sum(Ssm)>0);
    %initialise redundant eigenpairs, number of redundant eigenpairs
    Ured = zeros(size(Usm));
    Dred = zeros(size(Dsm));
    ne = 0;
    %for each eigenvalue
    e = round(diag(Dsm),dp);
    e_unique = unique(e);
    for j = 1:length(e_unique)
        %compute relevant null space and its dimension
        nullspace = null(round(Ssm'*Usm(:,e==e_unique(j)),dp));
        dim = size(nullspace,2);
        %store redundant eigenpairs
        if dim > 0
            range = ne+(1:dim);
            Ured(:,range) = ...
                Usm(:,e==e_unique(j))*nullspace;
            Dred(range,range) = e_unique(j) * eye(dim);
            %update number of redundant eigenpairs found
            ne = ne + dim;
        end
    end
    
    %check correct number of redudant eigenpairs found
    nexpected = length(sm) - length(orbits);
    if ne ~= nexpected
        warning(strcat('Motif %d: %d redundant eigenpairs found ',...
            '(%d expected)\n'), i, ne, nexpected);
    end

    %store Ured, Dred in U, D (uses m as last-position counter)
    range = m+(1:ne);
    U(sm,range) = Ured(:,1:ne);
    D(range,range) = Dred(1:ne,1:ne);
    m = m+ne;
end
if verbose, fprintf('Finished.\n'); toc; end

%return time
t = toc; 

%checks
if checkOutput
    disp('Checking decomposition...');
    ortherr = zeros(1,n);
    for i = 1:n
        ortherr(i) = norm(A*U(:,i)-D(i,i)*U(:,i),Inf);
        %fprintf('Eigenpair %d up to %g\n', i, ortherr(i));
    end
    fprintf('Eigenpairs correct up to (avg, max, min) %g %g %g\n',...
        mean(ortherr), max(ortherr), min(ortherr));
    fprintf('Matrix U orthogonal up to %g\n', norm(U*U'-eye(n),Inf));
    fprintf('Spectral decomposition up to %g\n', norm(A*U-U*D,Inf));
    fprintf('Decimal places %d\n', dp);
end

end


