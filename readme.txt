Code associated to 'Exploiting Symmetries in Complex Network Analysis' by Ruben J Sanchez-Garcia
Preprint available at https://arxiv.org/abs/1803.06915
Please cite this paper if you use or adapt the code

/networksymmetries/
  Geometric decomposition of the automorphism group of a network
/symmetrycompression/
  Matrix compression by eliminating symmetry-induced redundancies
/spectraldecomposition/
  Matrix eigendecomposition via symmetries
/datasets/
  Datasets analysed and generated in 'Exploiting Symmetries in Complex Network Analysis'

Please see inside each folder for further information

DATASETS
The datasets analysed and generated in 'Exploiting Symmetries in Complex Network Analysis' can be found at https://doi.org/10.6084/m9.figshare.11619792

