#!/bin/bash
## Analyse geometric decomposition from saucy output and disjoint support partition
## Computes type and list of orbits for each symmetric motif with GAP
## Input .gen and .gd files (generators and disjoint-support partition)
## Output .sm and .or file (types and orbits)
## Assumes .gd file sorted in ascending order
## Uses small generators to speed calculation up (suggested by Max Horn)
## GNU bash version 3.2.57(1)-release (x86_64-apple-darwin17)
## GAP 4.8 in /Applications/gap4r8/

# Check input arguments
if [ $# -lt 2 ]
then
	echo "Error ($0): you must supply two arguments"
	echo "Argument 1: file name (without .gen or .gd extension)"
	echo "Argument 2: number of vertices"
	exit 1
fi

## Convert saucy output (.gen) into GAP format temp.aut
cat "$1".gen | tr ' ' ',' | sed -e "s/(0,/($2,/g" -e "s/,0)/,$2)/" -e "s/,0,/,$2,/g" \
> temp.aut

## Analyse geometric_decomposition in GAP
echo -n "Starting GAP... "
/Applications/gap4r8/bin/gap.sh -r -b -q  << EOI
#verbose control flag
verbose := true;;
#print initial message
if verbose then Print("Analysing geometric decomposition with GAP...\n"); fi;
if verbose then Print("[Please ignore syntax warnings]\n"); fi;
#open input/output streams
stream_gens := InputTextFile("temp.aut");;
stream_gd := InputTextFile("$1.gd");;
stream_type := OutputTextFile("GAPoutput.sm",false);;
stream_orbits := OutputTextFile("GAPoutput.or",false);;
#start runtime
t1 := Runtime();;
#read one symmetric motif at a time
nextsm := EvalString(Chomp(ReadLine(stream_gd)));;
if verbose then count := 0;; fi;
while not IsEndOfStream(stream_gd) do
	sm := nextsm;;
	gens := [];;
	#extract generators
	while sm = nextsm do
		Append(gens,[EvalString(Chomp(ReadLine(stream_gens)))]);;
		if verbose then count := count + 1;; Print(count, " generators processed\r"); fi;
		line := ReadLine(stream_gd);
		if line = fail then break; fi;
		nextsm := EvalString(Chomp(line));;
	od;
	#check stream_gd non-decreasing
	if nextsm < sm then
		Error("\nInput file $1.gd must be in non-decreasing order");
	fi;
	#produce small generators
	mp := MovedPoints(gens);;
  	smallgens:=List(gens, g -> Permutation(g, mp));
	#geometric factor with small generators
	H := Group(smallgens);;
	#store orbits
	small_orbs := Orbits(H);;
	orbs:=List(small_orbs, o->mp{o});;
	WriteLine(stream_orbits,String(orbs));;
	#store factor type (0=complex motif, k=basic motif with geometric factor S_k)
	#complex motif unless orbits of same size and natural action on each orbit
	k := Minimum(List(small_orbs,Size));
	if k=Maximum(List(small_orbs,Size)) then
		type := k;;
		for orb in small_orbs do
			if not IsNaturalSymmetricGroup(Action(H,orb)) then
				type := 0;;
				break;
			fi;
		od;
	else
		type := 0;;
	fi;
	WriteLine(stream_type,String(type));;
od;
#close streams
CloseStream(stream_gens);;
CloseStream(stream_gd);;
CloseStream(stream_type);;
CloseStream(stream_orbits);;
#runtime
t2 := Runtime();;
Print("\nRuntime", StringTime(t2 - t1), "\n");
EOI
echo "Finished"

## Produce cleaned output files (.sm and .or)
mv GAPoutput.sm $1.sm
cat GAPoutput.or | tr -d ' ' | tr ',' ' ' | \
sed -e 's/\[\[/\[/' -e 's/]]/]/' -e 's/] \[/] * \[/g' > "$1".or

## Remove temporary files
rm -f temp.aut
rm -f GAPoutput.or
