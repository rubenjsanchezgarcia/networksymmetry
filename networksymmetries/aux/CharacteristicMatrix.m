function S = CharacteristicMatrix( partition, n )
%CHARACTERISTICMATRIX Characteristic matrix of a partition
%   partition is a partition of 1:n as a cell array with missing points
%   automatically added as singletons e.g. {[1 2 3] [5 6]} becomes 
%   {[1 2 3] [5 6] [4] [7]} if n==7
%   
%   S is the characteristic matrix of the partition S(i,j)=1 if vertex i
%   belongs to class j

%moved and fixed vertices
range = unique([partition{:}]); 
missing = setdiff(1:n,range);  

%number of non-trivial orbits and total number of orbits
no = length(partition);     
m = no + length(missing);  

%row vector (for sparse matrix creation) - vertex numbers
rv = [partition{:} missing];

%column vector (for sparse matrix creation) - class numbers
cv = repelem(1:no, cellfun(@length,partition)); %for moved vertices
cv = [cv no+1:m];                               %add singletons

%construct sparse characteristic matrix
S = sparse(rv, cv, true(1,n));

end

