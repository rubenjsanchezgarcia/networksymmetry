function [ adjmatrix, gdindex, motiftype, motifs, S ] = ReadGD( filename )
%READGD Reads geometric decomposition from data files into MATLAB
%   Reads filename.g (edge list), filename.gd (generator partition),
%   filename.sm (symmetric motif types) and filename.or (orbits) from
%   current folder and constructs:
%       adjmatrix   Sparse adjacency matrix
%       gdindex     Index vector for disjoint-support partition of 
%           generators: the kth generator (filename.gen) belongs to the
%           gdindex(k) motif
%       motiftype   motiftype(k)=n if the kth motif is of type n
%       motifs      Cell array of motifs: motifs{k} is the kth motif as a
%           cell array of orbits
%       S           characteristic matrix of the partition into orbits

%control flag
verbose = true;

%start timer
tic;

%import edge list substituing vertex label 0 by nv
if verbose, fprintf('Importing edge list... '); end
edgelist = dlmread(strcat(filename,'.g'));
nv = edgelist(1,1);     %number of vertices
ne = edgelist(1,2);     %number of edges
edgelist(edgelist==0) = nv;
if verbose, fprintf(' Done\n'); end

%construct sparse adjacency matrix
if verbose, fprintf('Constructing sparse adjacency matrix... '); end
adjmatrix = sparse(edgelist(2:end,1),edgelist(2:end,2),ones(1,ne),...
    nv,nv,ne); 
if verbose, fprintf(' Done\n'); end

%import geometric decomposition indices
if verbose, fprintf('Importing geometric decomposition... '); end
gdindex = dlmread(strcat(filename,'.gd'));
if verbose, fprintf(' Done\n'); end

%import symmetric motif types
if verbose, fprintf('Importing symmetric motif types... '); end
motiftype = dlmread(strcat(filename,'.sm'));
if verbose, fprintf(' Done\n'); end

%import symmetric motifs
if verbose, fprintf('Importing symmetric motifs... '); end
fid = fopen(strcat(filename,'.or'),'r');
L = textscan(fid,'%s','delimiter','\n');        %read lines
fclose(fid);
motifs = cell(size(L{1}));
for i = 1:length(L{1})
    C = textscan(L{1}{i},'%s','delimiter','*'); %separate orbits
    for j = 1:length(C{1})
        motifs{i}{j} = str2num(C{1}{j});        
    end
end
if verbose, fprintf(' Done\n'); toc; end

%characteristic matrix of the partition into orbits
if verbose, fprintf('Computing characteristic matrix...'); end
S = CharacteristicMatrix([motifs{:}],nv);
if verbose, fprintf(' Done\n'); toc; end

end

