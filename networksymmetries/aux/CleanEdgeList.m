function [EdgeList, ConversionVector] = CleanEdgeList( EdgeListIn, filename )
%CLEANEDGELIST Cleans edge list
%   EdgeListIn a 2-column integer matrix representing edges as vertex pairs
%   The output EdgeList has the same format but:
%       self-loops and duplicated edges are eliminated
%       vertices outside the largest connected component are removed
%       vertices are renumbered 0 to number of vertices minus 1  
%   
%   If optional filename is provided, the output EdgeList is written in 
%   filename.g in current folder
%
%   ConversionVector is a conversion vector of vertex labels so vertex i in
%   EdgeList is vertex ConversionVector(i+1) in EdgeListIn
%
%   This function accepts a 2-column table input, which is first converted
%   to a 2-column integer matrix of unique indices

% control flags
verbose = true;
extractlcc = true;  

% start timer
tic;

% covert table input to numeric
if istable(EdgeListIn) && size(EdgeListIn,2) == 2
    indices = grp2idx([EdgeListIn{:,1}; EdgeListIn{:,2}]);  %unique indices
    nr = size(EdgeListIn,1);                                %number of rows
    EdgeListIn = [indices(1:nr) indices(nr+1:end)];
end

% input network
if verbose
    fprintf('\nInput network has %d vertices and %d edges\n\n',...
    length(unique(EdgeListIn(:))), size(EdgeListIn,1));
end

% remove self-loops
if verbose, fprintf('Removing self-loops... '); end
EdgeList = EdgeListIn(EdgeListIn(:,1)~=EdgeListIn(:,2),:);
if verbose
    fprintf('%d removed\n', size(EdgeListIn,1)-size(EdgeList,1)); 
end

% vertices in self-loops only
slv = length(unique(EdgeListIn(:)))-length(unique(EdgeList(:)));
if verbose && slv > 0
    fprintf('Removing vertices in self-loops only... ');
    fprintf('%d removed\n', slv);
end

% eliminate duplicate edges
if verbose, fprintf('Removing duplicate edges... '); end
aux = EdgeList;
EdgeList = sort(EdgeList,2); 
EdgeList = unique(EdgeList,'rows');
if verbose
    fprintf('%d removed\n', size(aux,1)-size(EdgeList,1) );
end

% extract largest connected component (lcc)
if extractlcc
    if verbose, fprintf('Extracting largest connected component... '); end
    nodes = unique(EdgeList(:));
    adjmatrix = sparse(EdgeList(:,1),EdgeList(:,2),...
        ones(size(EdgeList(:,1))), max(nodes), max(nodes));
    [ncc,C] = graphconncomp(adjmatrix,'weak',true);
    if verbose, fprintf('%d found\n', ncc); end
    lccidx = mode(C);                   %index of nodes in lcc
    lcc = find(C==lccidx);              %nodes in lcc
    if verbose
        fprintf('Largest connected component of size %d (%.2f%%)\n',...
            length(lcc), length(lcc)/length(nodes)*100);
    end  
    %find edges not in lcc
    if verbose, fprintf('Removing edges not in lcc... '); end
    notlcc = setdiff(nodes,lcc);            %nodes not in lcc
    remove = false(size(EdgeList,1),1);     %rows to remove from edge list
    for i=1:length(notlcc)
        remove = remove | EdgeList(:,1)==notlcc(i);
    end  
    if verbose, fprintf('%d removed\n', nnz(remove)); end
    %update edge list
    EdgeList = EdgeList(not(remove),:);
end

% node list, number of vertices and number of edges
nodes = unique(EdgeList(:));
nv = length(nodes);
ne = size(EdgeList,1);

% conversion vector
if verbose, fprintf('\nCreating conversion vector... '); end
ConversionVector = nodes;
if verbose, fprintf('Done\n'); end

% label nodes 0 to nv-1 via inverse conversion vector
ict = sparse(nodes,ones(nv,1),0:nv-1);
if verbose, fprintf('Relabelling nodes 0 to %d... ', nv-1); end
for i=1:ne
    s = full(ict(EdgeList(i,1)));   %source
    t = full(ict(EdgeList(i,2)));   %target
    EdgeList(i,:)=[s t];
end
if verbose, fprintf('Done\n'); end

% writes output in current folder if filename provided
if nargin > 1
    outputfile = strcat(filename,'.g'); 
    fprintf('\nWriting output file %s in current folder... ',outputfile);
    dlmwrite(outputfile,[nv ne 1], 'delimiter', ' ', 'precision', '%.0f');
    dlmwrite(strcat(filename,'.g'),EdgeList, 'delimiter', ' ','-append',...
        'precision', '%.0f');
    fprintf('Done\n');
end

% statistics
if verbose
    fprintf('\nOutput network has %d vertices and %d edges\n\n', nv, ne);
    fprintf('Total time %.2f seconds\n\n',toc);
end

end

