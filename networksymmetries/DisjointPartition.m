function partition = DisjointPartition( R, n, sorting )
%DISJOINTPARTITION Partition rows into disjoint classes
%   DisjointPartition( R ) partition the rows of a matrix R
%   Each row of the matrix R must be an array of non-negative integers,
%   possibly with repetitions, followed by zeros. Two rows are called 
%   disjoint if they do not have elements in common apart from 0.
%   
%   DisjointPartition partitions the rows into classes such that rows in
%   different classes are disjoint and it is the finest such partition (no
%   further subpartition has this property).
%
%   The output partition is a vector of class numbers, that is, 
%   partition(i) is the class number of the ith row.
%
%   DisjointPartition( filename, n ) reads the matrix R from the saucy 
%   output file filename.gen, and n is the total number of vertices. It
%   saves the result in filename.gd
%
%   DisjointPartition( filename, n, true ) additionally sorts the output so
%   that filename.gd is in non-decreasing order. The input file
%   filename.gen is sorted accordingly and overwritten. 

%control flags
verbose = true;

%start timer
tic;

%input
if nargin > 1
    %read input file via auxiliary file
    inputfilename = strcat(R,'.gen');
    if verbose, fprintf('\nReading %s... ', inputfilename); end
    unix(['cat ', inputfilename, ' | tr ''()'' '' '' | sed "s/ 0 / ',...
        num2str(n), ' /g"> ', R, '.gen.aux']); %remove (), replace 0 by n
    L = dlmread(strcat(R,'.gen.aux'));
    unix(['rm -f ', strcat(R,'.gen.aux')]); %delete auxiliary file
else
    L = R;
end
if verbose, fprintf('Done\n'); end

%conversion table (converts all non-zero integers in L to range 1:ni)
if verbose, fprintf('Coversion table... '); end
nr = size(L,1); 
nmax = max(L(:));    
ct = zeros(1,nmax); 
for i = 1:nr
    ct(L(i,L(i,:)>0)) = 1; 
end
ni = nnz(ct);   %number of unique integers in all lists
ct(ct>0) = 1:ni; 
if verbose, fprintf('Done\n'); end

%store lists in bipartite graph form
if verbose, fprintf('Store lists in bipartite graph form... '); end
rv = repelem(1:nr,sum(sign(L),2));  %row vector for sparse adj matrix
Lt = transpose(L);                  %auxiliary transpose matrix
cv = ct(Lt(Lt>0));                  %column vector for sparse adj matrix
nv = nr+max(cv);                    %number of vertices
A = sparse(rv,cv+nr,true(1,nnz(L)),nv,nv);    %adj matrix
if verbose, fprintf('Done\n'); toc; end

%extract connected components
if verbose, fprintf('Extract connected components... '); end
[~,C] = graphconncomp(A,'weak',true);
partition = C(1:nr)';
if verbose, fprintf('Done\n'); toc; end
    
%save partition to file
if nargin > 1
    outputfilename = strcat(R,'.gd');
    if verbose, fprintf('\nWriting %s...', outputfilename); end
    dlmwrite(outputfilename, partition, 'delimiter', '\n',...
        'precision', '%.0f');
end
if verbose, fprintf('Done\n'); toc; end

%sort partition and generators
if nargin > 2 && sorting
    if verbose, fprintf('\nSorting partition and generators...'); end
    unix(['paste ', outputfilename, ' ', inputfilename,...
        ' | sort -g | tee >(cut -f1 > ',...
        strcat(outputfilename,'.sorted'), ') | cut -f2 > ',...
        strcat(inputfilename,'.sorted')]);
    partition = dlmread(strcat(outputfilename,'.sorted')); 
    if verbose, fprintf(['Done\nOutput written in files %s.sorted ',...
            'and %s.sorted\n'], inputfilename, outputfilename); toc; end 
end
    
end

