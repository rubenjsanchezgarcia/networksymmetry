Code associated to 'Exploiting Symmetries in Complex Network Analysis' by Ruben J Sanchez-Garcia
Preprint available at https://arxiv.org/abs/1803.06915
Please cite this paper if you use or adapt the code

/example/
  Toy example of a network with 33 nodes
    toy.pdf   Network plot (nodes labelled 1 to 33)
    toy.g     Edge list in saucy input (nodes labelled 0 to 32)
    toy.gen   List of generators in saucy output format
    toy.gd    Disjoint-support partition of generators
    toy.or    List of symmetric motifs with orbits separated by '*'
    toy.sm    List of symmetric motif types

COMMANDS ($ shell, > Matlab)
$ ./saucy toy.g > toy.gen               Creates toy.gen
> DisjointPartition('toy', 33);         Creates toy.gd
$ geometric_decomposition.sh toy 33     Creates toy.or and toy.sm

See /networksymmetries/instructions.txt for further details
